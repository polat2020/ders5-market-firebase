package com.polat.marketim;

public class MarketDto {

    private String email;
    private String detay;
    private String url;

    public MarketDto(String email, String detay, String url) {
        this.email = email;
        this.detay = detay;
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public String getDetay() {
        return detay;
    }

    public String getUrl() {
        return url;
    }

}
