package com.polat.marketim;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.polat.marketim.databinding.ActivityMarketBinding;

import java.util.ArrayList;
import java.util.Map;

public class MarketActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;
    ArrayList<MarketDto> list;
    MarketRecyclerAdapter adapter;
    ActivityMarketBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  ActivityMarketBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        list = new ArrayList<>();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        ListeGetir();

        SetAdapter();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.market_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.marketEkle) {
            Intent intent = new Intent(MarketActivity.this, UploadActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.cikisYap) {

                firebaseAuth.signOut();

                Intent intentToSignUp = new Intent(MarketActivity.this, SignUpActivity.class);
                startActivity(intentToSignUp);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }




    public void ListeGetir() {
        firebaseFirestore.collection("Market").orderBy("tarih", Query.Direction.DESCENDING)
                .addSnapshotListener((queryDocumentSnapshots, e) -> {

            if (e != null) {
                Toast.makeText(MarketActivity.this,e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
                return;
            }

            if (queryDocumentSnapshots != null) {

                for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                    Map<String,Object> data = snapshot.getData();
                    MarketDto marketDto = new MarketDto((String) data.get("email"),(String) data.get("detay"),(String) data.get("url"));
                    list.add(marketDto);
                }
                adapter.notifyDataSetChanged();

            }
        });
    }
    private void SetAdapter() {
        adapter = new MarketRecyclerAdapter(list);
        binding.recyclerView.setAdapter(adapter);
    }


}
