package com.polat.marketim;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.polat.marketim.databinding.ActivitySignupBinding;

public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private ActivitySignupBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignupBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        firebaseAuth = FirebaseAuth.getInstance();

        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser != null) {

            Intent intent = new Intent(SignUpActivity.this, MarketActivity.class);
            startActivity(intent);
            finish();

        }
    }

    public void signIn(View view) {

        String email = binding.txtEmail.getText().toString();
        String password = binding.txtPassword.getText().toString();

        firebaseAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(authResult -> {

            Intent intent = new Intent(SignUpActivity.this, MarketActivity.class);
            startActivity(intent);
            finish();
        }).addOnFailureListener(e ->
                Toast.makeText(SignUpActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show()
        );
    }

    public void signUp(View view) {

        String email = binding.txtEmail.getText().toString();
        String password = binding.txtPassword.getText().toString();

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(authResult -> {

            Toast.makeText(SignUpActivity.this, "Kullanıcı Oluşturuldu", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(SignUpActivity.this, MarketActivity.class);
            startActivity(intent);
            finish();

        }).addOnFailureListener(e ->
                Toast.makeText(SignUpActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG)
                        .show());


    }
}
