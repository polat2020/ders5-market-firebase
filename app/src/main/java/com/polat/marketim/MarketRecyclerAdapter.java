package com.polat.marketim;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.polat.marketim.databinding.RecyclerRowBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MarketRecyclerAdapter extends RecyclerView.Adapter<MarketPostHolder> {

    private final ArrayList<MarketDto> marketDtoArrayList;

    public MarketRecyclerAdapter(ArrayList<MarketDto> marketDtoArrayList) {
        this.marketDtoArrayList = marketDtoArrayList;
    }

    @NonNull
    @Override
    public MarketPostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerRowBinding recyclerRowBinding = RecyclerRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MarketPostHolder(recyclerRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MarketPostHolder holder, int position) {

        holder.recyclerRowBinding.txtRecyclerEmail.setText(marketDtoArrayList.get(position).getEmail());
        holder.recyclerRowBinding.txtRecyclerDetay.setText(marketDtoArrayList.get(position).getDetay());
        Picasso.get().load(marketDtoArrayList.get(position).getUrl()).into(holder.recyclerRowBinding.imgRecyclerImage);
    }

    @Override
    public int getItemCount() {
        return marketDtoArrayList.size();
    }


}
