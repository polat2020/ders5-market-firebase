package com.polat.marketim;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.polat.marketim.databinding.RecyclerRowBinding;

public class MarketPostHolder extends RecyclerView.ViewHolder {
    RecyclerRowBinding recyclerRowBinding;

    public MarketPostHolder(@NonNull RecyclerRowBinding recyclerRowBinding) {
        super(recyclerRowBinding.getRoot());
        this.recyclerRowBinding = recyclerRowBinding;

    }
}
